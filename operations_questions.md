

Questions mostly about rpc calls from /blocks/{}/proto/operations


# Q: Endorsments have Slots, what are Slots?

Example:

```
 {
        "hash": "ooDACKvL5MBwXv2MVs4Upk54wS36RQTY2KKe4XwkbR4Cithe3de",
        "branch": "BLNGYV28sZgeEh6rsWpMuHJ7iLCSG1Qfy6MjzbGDJxpkP2DsUDU",
        "source": "edpkuq1nqG9ahchLjdb7XxkCE79o8FanAj3naNmN7cC9Dv6QUn4uTe",
        "operations": [
          {
            "kind": "endorsement",
            "block": "BLNGYV28sZgeEh6rsWpMuHJ7iLCSG1Qfy6MjzbGDJxpkP2DsUDU",
            "slot": 14 <<----
          }
        ],
        "signature": "edsigu1CLiuSwE2qEhCDbAGbK9QrxeFFheuy9FhBcA9Ua6HBazoWEFizWFenzg9VZ1y1FSNFZiUukf8Ro9ah8heTgWLXgWcVW1P"
      },
```


# Q: Different kinds of Operations have different fields, like for example

```
      {
        "hash": "op9xy3fbNUTnuan2vf47nPR5W96R1btd2V85ZoDy2fCJV8C7y3c",
        "branch": "BLxYFaCyS6JjRG4T7FeBcFjiPKDvs7o9XE8Qd2DygFoR5Ff4D2R",
        "operations": [
          {
            "kind": "faucet",
            "id": "tz1PMpMfzdxvqdQoPKwE2je6U4RY2CyF5V7k",
            "nonce": "347fcee01ccd14e3907707e972cfea33"
          }
        ]
      },
      {
        "hash": "onhBVNproYFuQypX9YaqboKzJuJmTy162rN7NNkBtM5rcbD21K3",
        "branch": "BLNGYV28sZgeEh6rsWpMuHJ7iLCSG1Qfy6MjzbGDJxpkP2DsUDU",
        "source": "edpkuq1nqG9ahchLjdb7XxkCE79o8FanAj3naNmN7cC9Dv6QUn4uTe",
        "operations": [
          {
            "kind": "endorsement",
            "block": "BLNGYV28sZgeEh6rsWpMuHJ7iLCSG1Qfy6MjzbGDJxpkP2DsUDU",
            "slot": 10
          }
        ],
        "signature": "edsigtosPDWykZYdoJPz8ejRQGYVzGby8HFZeATt6TywjTgyx3pCNih2UaW59e3tGBvRfqAUqiWnsWBmVDLRwChowb6PpV7WgeX"
      },
```

so faucet has id and nonce,
endormsement has block and slot...
Is there a List of all allowed fields?

# Q: what exactly is an endorsment?

# Q: Transactions include Counter, what are Counters?

```
{
        "hash": "op5tUftP6ut9EQks7Enzq7y8bFwzpWwiEDJv1HQ92h8bzW5RKLi",
        "branch": "BLxYFaCyS6JjRG4T7FeBcFjiPKDvs7o9XE8Qd2DygFoR5Ff4D2R",
        "source": "TZ1a636mfuBMhTyjtsjGVSD1m8eQ4y4T4khs",
        "public_key": "edpkuS1od7K64LvBsaxZgkem4KkDdZRz195erqid2bBugKyGBCvq4W",
        "fee": 0,
        "counter": 1,  <<----
        "operations": [
          {
            "kind": "transaction",
            "amount": 10000000,
            "destination": "tz1XA5Bc1qG64ju7dUbPmZpriGfLzCDxvKdS"
          }
        ],
        "signature": "edsigu6fSiAMwzAG1yUmXPQuB3uNYxXZTjRFfVkiY1ge842dDPeLVS5BHL7TyT1kUB1Uz24zwQUaEkPLBw8gC1P62iUVM2rDZLp"
      }
```


# Q: Delegations do not include the amount that was delegated, how do i get the amount?

```
{
        "hash": "op9QkW1wutFrZ6yzx9T3es2si4hecMb4gLSYyxXZ6k5e8E4fWcy",
        "branch": "BLfnWuDn1YCx4EQm2Hg35Rt7JDjXRGPebf2a6Vb9YYEmTLFc1zD",
        "source": "TZ1qfzLXSyPrfVhSmk61Mz32JScrpgUjq7vE",
        "public_key": "edpkvJSM1UgxoKqjsaNekgjabwVWixbQapsRriv3mXWdTgp8NmpiRq",
        "fee": 5,
        "counter": 2,
        "operations": [
          {
            "kind": "delegation",
            "delegate": "tz1Yj2kGBWazrZwaJEwdJpB3eVEA1FuUPdbe"
          } ????????????????
        ],
        "signature": "edsigtbatzxLtTZu2PwabLAB5wYFyEFMGguiDgeR9ugCUXs7SvjuieVt5oJeTTrSAcfcLt3iVh5XPwtshPme45hf2ZFSgcFy97G"
      }
```


# Q: What are Originations?

```
      {
        "hash": "onvYYqLwttupUxfEn6Tu747wp94hCe7azz5MoD7AaSxqs9bCXjJ",
        "branch": "BLyHFxQ993Z68athEy9Ght45NQCXsW5oveJznSAeEwggEn6tUp3",
        "source": "TZ1bDjjCQnJ7fEEGuUrbiC4ySDraQLdEU1E4",
        "public_key": "edpktoFtm4ZgKHd8roLgWTpfEU9mZVv5VFnceHphhBQKGPTFVJ8tSN",
        "fee": 5,
        "counter": 4,
        "operations": [
          {
            "kind": "origination",
            "managerPubkey": "tz1Z9NQTQDuSSjoq8m3dWn3awQg1jQmzcWiS",
            "balance": 150000,
            "spendable": true,
            "delegatable": false
          }
        ],
        "signature": "edsigtssjmsTqHmYKs9ufn43V11JvhW2RhnuQjnqUBdRNM8vuFVNmPr429fPq6LXLJ8YrydJFVuJPo94eU48YM4Qs8BBcsZMTXb"
      }
```

